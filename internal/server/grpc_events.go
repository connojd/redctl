// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package server

import (
	"log"
	"sync"

	"github.com/rs/xid"

	"gitlab.com/redfield/redctl/api"
)

func (s *server) SubscribeEvents(r *api.SubscribeEventsRequest, stream api.Redctl_SubscribeEventsServer) error {
	el := s.eventPublisher.registerSubscriber()
	defer s.eventPublisher.unregisterSubscriber(el)

	for {
		event := <-el.update

		r := &api.SubscribeEventsReply{Event: &event}
		if err := stream.Send(r); err != nil {
			log.Printf("fatal error in session event handler: %v", err)
			return err
		}
	}
}

func (s *server) publishDomainEvent(et api.Event_Type, dom *api.Domain) {
	if s.eventPublisher == nil {
		return
	}
	e := api.Event{
		Type: et,
		Data: &api.Event_Domain{Domain: dom},
	}
	s.eventPublisher.update <- e
}

func (s *server) publishDomainNetworkEvent(et api.Event_Type, dom *api.Domain, net *api.DomainNetwork) {
	if s.eventPublisher == nil {
		return
	}
	e := api.Event{
		Type:   et,
		Data:   &api.Event_Domain{Domain: dom},
		Device: &api.Event_Network{Network: net},
	}
	s.eventPublisher.update <- e
}

type publisher struct {
	update chan api.Event // Main update channel

	mux         sync.Mutex
	subscribers map[string]chan api.Event // Subscriber channels, e.g. UI
}

type subscriber struct {
	update chan api.Event
	id     string
}

func (b *publisher) registerSubscriber() *subscriber {
	// Store the new subscriber channel
	id := xid.New().String()
	uc := make(chan api.Event)

	b.mux.Lock()
	b.subscribers[id] = uc
	b.mux.Unlock()

	lis := subscriber{
		update: uc,
		id:     id,
	}

	return &lis
}

func (b *publisher) unregisterSubscriber(sub *subscriber) {
	// Remove the subscriber
	b.mux.Lock()
	delete(b.subscribers, sub.id)
	b.mux.Unlock()
}

func (b *publisher) serve() {
	for {
		update := <-b.update

		// Publish update to subscribers
		b.mux.Lock()
		for _, v := range b.subscribers {
			v <- update
		}
		b.mux.Unlock()
	}
}

func newPublisher() *publisher {
	b := publisher{
		update:      make(chan api.Event, 100),
		subscribers: make(map[string]chan api.Event),
	}

	return &b
}
