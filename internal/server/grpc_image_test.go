// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package server

import (
	"context"
	"io/ioutil"
	"os"
	"path/filepath"
	"testing"

	"gitlab.com/redfield/redctl/api"
	"gitlab.com/redfield/redctl/internal/server/repository"
)

func TestImageCreate(t *testing.T) {
	dir, err := ioutil.TempDir("", "")
	if err != nil {
		t.Fatalf("Failed to create temp directory: %v\n", err)
	}
	defer os.RemoveAll(dir)

	repo := repository.NewFilesystemImageRepo(dir)
	s := server{imageRepo: repo}

	image := api.Image{Name: "test1.qcow2", Size: 1000000}
	req := api.ImageCreateRequest{Image: &image}

	reply, err := s.ImageCreate(context.Background(), &req)
	if err != nil || reply == nil {
		t.Fatalf("Failed to create test1.qcow2: %v\n", err)
	}

	req2 := api.ImageFindRequest{Name: "test1.qcow2"}
	reply2, err2 := s.ImageFind(context.Background(), &req2)
	if err2 != nil || reply2 == nil {
		t.Fatalf("Failed to find test1.qcow2: %v\n", err2)
	}
}

func TestImageCreateBacked(t *testing.T) {
	dir, err := ioutil.TempDir("", "")
	if err != nil {
		t.Fatalf("Failed to create temp directory: %v\n", err)
	}
	defer os.RemoveAll(dir)

	repo := repository.NewFilesystemImageRepo(dir)
	s := server{imageRepo: repo}

	backingImage := api.Image{Name: "test1_backing.qcow2", Size: 1000000}
	req := api.ImageCreateRequest{Image: &backingImage}
	reply, err := s.ImageCreate(context.Background(), &req)
	if err != nil || reply == nil {
		t.Fatalf("Failed to create test1_backing.qcow2: %v\n", err)
	}

	image := api.Image{Name: "test1.qcow2"}
	req2 := api.ImageCreateBackedRequest{Image: &image, BackingImage: &backingImage}
	reply2, err2 := s.ImageCreateBacked(context.Background(), &req2)
	if err2 != nil || reply2 == nil {
		t.Fatalf("Failed to create backed test1.qcow2: %v\n", err2)
	}

	req3 := api.ImageFindRequest{Name: "test1.qcow2"}
	reply3, err3 := s.ImageFind(context.Background(), &req3)
	if err3 != nil || reply3 == nil {
		t.Fatalf("Failed to find test1.qcow2: %v\n", err3)
	}
}

func TestImageFind(t *testing.T) {
	dir, err := ioutil.TempDir("", "")
	if err != nil {
		t.Fatalf("Failed to create temp directory: %v\n", err)
	}
	defer os.RemoveAll(dir)

	repo := repository.NewFilesystemImageRepo(dir)
	s := server{imageRepo: repo}

	var req api.ImageFindRequest
	req.Name = ""
	reply, err := s.ImageFind(context.Background(), &req)
	if err != nil {
		t.Fatalf("Failed to FindImages for empty repo: %v\n", err)
	}

	if len(reply.Images) > 0 {
		t.Fatalf("Found images for empty repo: %v\n", reply.Images)
	}

	req.Name = "nothere"
	reply, err = s.ImageFind(context.Background(), &req)
	if err != nil {
		t.Fatalf("Failed to FindImages for empty repo: %v\n", err)
	}

	if reply.Images != nil {
		t.Fatalf("Found images for empty repo: %v\n", reply.Images)
	}

	_, err = os.OpenFile(filepath.Join(dir, "image1.qcow2"), os.O_RDONLY|os.O_CREATE, 0666)
	if err != nil {
		t.Fatalf("failed to create image1.qcow2: %v\n", err)
	}

	_, err = os.OpenFile(filepath.Join(dir, "image2.iso"), os.O_RDONLY|os.O_CREATE, 0666)
	if err != nil {
		t.Fatalf("failed to create image1.qcow2: %v\n", err)
	}

	_, err = os.OpenFile(filepath.Join(dir, "image3.raw"), os.O_RDONLY|os.O_CREATE, 0666)
	if err != nil {
		t.Fatalf("failed to create image1.qcow2: %v\n", err)
	}

	req.Name = "image2.iso"
	reply, err = s.ImageFind(context.Background(), &req)
	if err != nil {
		t.Fatalf("Failed to FindImages for populated repo: %v\n", err)
	}

	if reply.Images == nil {
		t.Fatalf("Images is nil for populated repo: %v\n", reply.Images)
	}
}

func TestImageRemove(t *testing.T) {
	dir, err := ioutil.TempDir("", "")
	if err != nil {
		t.Fatalf("Failed to create temp directory: %v\n", err)
	}
	defer os.RemoveAll(dir)

	repo := repository.NewFilesystemImageRepo(dir)
	s := server{imageRepo: repo}

	var req api.ImageRemoveRequest

	req.Name = ""
	_, err = s.ImageRemove(context.Background(), &req)
	if err == nil {
		t.Fatalf("Should error on removing empty file name for empty repo\n")
	}

	req.Name = "nothere"
	_, err = s.ImageRemove(context.Background(), &req)
	if err == nil {
		t.Fatalf("Should error on removing invalid file name for empty repo\n")
	}

	_, err = os.OpenFile(filepath.Join(dir, "image1.qcow2"), os.O_RDONLY|os.O_CREATE, 0666)
	if err != nil {
		t.Fatalf("failed to create image1.qcow2: %v\n", err)
	}
	_, err = os.OpenFile(filepath.Join(dir, "image2.iso"), os.O_RDONLY|os.O_CREATE, 0666)
	if err != nil {
		t.Fatalf("failed to create image2.iso: %v\n", err)
	}
	_, err = os.OpenFile(filepath.Join(dir, "image3.raw"), os.O_RDONLY|os.O_CREATE, 0666)
	if err != nil {
		t.Fatalf("failed to create image3.raw: %v\n", err)
	}

	req.Name = "image2.iso"
	_, err = s.ImageRemove(context.Background(), &req)
	if err != nil {
		t.Fatalf("Failed to remove image for populated repo: %v\n", err)
	}
}
