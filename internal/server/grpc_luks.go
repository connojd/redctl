// Copyright © 2018 Assured Information Security, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package server

import (
	"context"
	"errors"
	"io/ioutil"
	"log"
	"os"
	"os/exec"

	"gitlab.com/redfield/redctl/api"
)

func tempKeyFile(bytes []byte) (string, error) {
	tmpFile, err := ioutil.TempFile(os.TempDir(), "kf-")
	if err != nil {
		return "", err
	}

	path := tmpFile.Name()

	err = ioutil.WriteFile(path, bytes, 0600)
	if err != nil {
		return "", err
	}

	return path, nil
}

func scrubKeyFile(path string) {
	// always attempt removal
	defer os.Remove(path)

	file, err := os.OpenFile(path, os.O_RDWR, 0666)
	if err != nil {
		log.Printf("Failed to scrub keyfile (%v) - openfile failed with: %v\n", path, err)
		return
	}
	defer file.Close()

	fileInfo, err := file.Stat()
	if err != nil {
		log.Printf("Failed to scrub keyfile (%v) - stat failed with: %v\n", path, err)
		return
	}

	size := fileInfo.Size()

	junk := make([]byte, size)

	n, err := file.Write(junk)
	if err != nil {
		log.Printf("Failed to scrub keyfile (%v) - write failed with: %v\n", path, err)
	}

	if int64(n) < size {
		log.Printf("Short changed on scrubbing keyfile - overwrote %v of %v bytes\n", n, size)
	}
}

func cryptsetupLuksAddKey(ek, nk []byte) error {
	ekf, err := tempKeyFile(ek)
	if err != nil {
		return err
	}
	defer scrubKeyFile(ekf)

	nkf, err := tempKeyFile(nk)
	if err != nil {
		return err
	}
	defer scrubKeyFile(nkf)

	out, err := exec.Command("cryptsetup", "luksAddKey", "/dev/dom0/storage", nkf, "--key-file", ekf).CombinedOutput()
	if err != nil {
		log.Printf("failed to exec cryptsetup luksAddKey: %v (err = %v)\n", out, err)
		return err
	}

	log.Println("added LUKS key with cryptsetup")
	return nil
}

func cryptsetupLuksChangeKey(ek, nk []byte) error {
	ekf, err := tempKeyFile(ek)
	if err != nil {
		return err
	}
	defer scrubKeyFile(ekf)

	nkf, err := tempKeyFile(nk)
	if err != nil {
		return err
	}
	defer scrubKeyFile(nkf)

	out, err := exec.Command("cryptsetup", "luksChangeKey", "/dev/dom0/storage", nkf, "--key-file", ekf).CombinedOutput()
	if err != nil {
		log.Printf("failed to exec cryptsetup luksChangeKey: %v (err = %v)\n", out, err)
		return err
	}

	log.Println("changed LUKS key with cryptsetup")
	return nil
}

func cryptsetupLuksRemoveKey(ek []byte) error {
	ekf, err := tempKeyFile(ek)
	if err != nil {
		return err
	}
	defer scrubKeyFile(ekf)

	out, err := exec.Command("cryptsetup", "luksRemoveKey", "/dev/dom0/storage", ekf).CombinedOutput()
	if err != nil {
		log.Printf("failed to exec cryptsetup luksRemoveKey: %v (err = %v)\n", out, err)
		return err
	}

	log.Println("removed LUKS key with cryptsetup")
	return nil
}

func (s *server) LuksAddKey(ctx context.Context, in *api.LuksAddKeyRequest) (*api.LuksAddKeyReply, error) {
	ek := in.GetExistingKey()
	nk := in.GetNewKey()

	if len(nk) < 8 {
		return &api.LuksAddKeyReply{}, errors.New("new key too short")
	}

	err := cryptsetupLuksAddKey(ek, nk)
	return &api.LuksAddKeyReply{}, err
}

func (s *server) LuksRemoveKey(ctx context.Context, in *api.LuksRemoveKeyRequest) (*api.LuksRemoveKeyReply, error) {
	rk := in.GetRemoveKey()

	err := cryptsetupLuksRemoveKey(rk)
	return &api.LuksRemoveKeyReply{}, err
}

func (s *server) LuksRotateKey(ctx context.Context, in *api.LuksRotateKeyRequest) (*api.LuksRotateKeyReply, error) {
	ek := in.GetExistingKey()
	nk := in.GetNewKey()

	if len(nk) < 8 {
		return &api.LuksRotateKeyReply{}, errors.New("new key too short")
	}

	err := cryptsetupLuksChangeKey(ek, nk)
	return &api.LuksRotateKeyReply{}, err
}
