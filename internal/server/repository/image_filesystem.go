// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package repository

import (
	"errors"
	"fmt"
	"io"
	"log"
	"os"
	"os/exec"
	"path"

	"io/ioutil"
	"path/filepath"

	"gitlab.com/redfield/redctl/api"
)

const (
	FileExtQCOW = ".qcow2"
	FileExtRAW  = ".raw"
	FileExtISO  = ".iso"
)

type filesystemImageRepo struct {
	path string
}

// NewFilesystemImageRepo for repository resident in filesystem (images path)
func NewFilesystemImageRepo(path string) ImageRepo {
	log.Println("instantiating image repo at:", path)
	return filesystemImageRepo{path: path}
}

func (s filesystemImageRepo) ImageAbsPath(name string) string {
	return filepath.Join(s.path, name)
}

func (s filesystemImageRepo) Find(name string) (*api.Image, error) {
	if name == "" {
		return nil, errors.New("Find requires non-empty (valid) image name")
	}

	fi, err := os.Stat(s.ImageAbsPath(name))
	if err != nil {
		// if image doesn't exist, it's not an error
		if os.IsNotExist(err) {
			return nil, nil
		}

		// other errors should propagate
		return nil, err
	}

	image, err := NewImage(fi)
	if err != nil {
		return nil, err
	}
	image.Path = s.ImageAbsPath(image.Name)

	return image, nil
}

// NewImage for fileinfo
func NewImage(fi os.FileInfo) (*api.Image, error) {
	var image api.Image
	image.Size = fi.Size()
	image.Name = fi.Name()

	switch ext := path.Ext(fi.Name()); ext {
	case FileExtQCOW:
		image.Format = api.ImageFormat_QCOW2
	case FileExtRAW:
		image.Format = api.ImageFormat_RAW
	case FileExtISO:
		image.Format = api.ImageFormat_ISO
	default:
		return nil, errors.New("unknown image extension: " + ext)
	}

	return &image, nil
}

func (s filesystemImageRepo) FindAll() ([]*api.Image, error) {
	images := make([]*api.Image, 0)

	files, err := ioutil.ReadDir(s.path)
	if err != nil {
		log.Fatal(err)
	}

	for _, f := range files {
		fi, err := NewImage(f)
		if err != nil {
			log.Printf("image repository processing error on %v: %v\n", f, err)
			continue
		}

		images = append(images, fi)
	}

	return images, nil
}

func (s filesystemImageRepo) Remove(name string) error {
	if name == "" {
		return errors.New("request to remove image with empty string")
	}

	return os.Remove(s.ImageAbsPath(name))
}

func (s filesystemImageRepo) createQCOW2Image(path string, backingFile string, bytes int64) error {
	args := []string{"create", "-f", "qcow2"}

	if backingFile != "" {
		args = append(args, "-b")
		args = append(args, backingFile)
	}

	args = append(args, path)

	if bytes > 0 {
		args = append(args, fmt.Sprintf("%d", bytes))
	}

	return exec.Command("qemu-img", args...).Run()
}

func (s filesystemImageRepo) createRAWImage(path string, bytes int64) error {
	_, err := os.OpenFile(path, os.O_CREATE, 0666)
	if err != nil {
		return err
	}
	return os.Truncate(path, bytes)
}

func (s filesystemImageRepo) Create(name string, bytes int64) (*api.Image, error) {
	image, err := s.Find(name)
	if err != nil {
		return nil, err
	}

	if image != nil {
		return nil, errors.New("image already exists")
	}

	switch format := path.Ext(name); format {
	case FileExtQCOW:
		err = s.createQCOW2Image(s.ImageAbsPath(name), "", bytes)
		if err != nil {
			return nil, err
		}
	case FileExtRAW, FileExtISO:
		err = s.createRAWImage(s.ImageAbsPath(name), bytes)
		if err != nil {
			return nil, err
		}
	default:
		return nil, errors.New("image name must have .qcow2, .raw or .iso extension")
	}

	return s.Find(name)
}

func (s filesystemImageRepo) CreateBacked(name string, backingImageName string, bytes int64) (*api.Image, error) {
	image, err := s.Find(backingImageName)
	if err != nil {
		return nil, err
	}

	if image == nil {
		return nil, errors.New("backing image not found")
	}

	image, err = s.Find(name)
	if err != nil {
		return nil, err
	}

	if image != nil {
		return nil, errors.New("image already exists")
	}

	if path.Ext(name) != FileExtQCOW {
		return nil, errors.New("image name must have .qcow2 extension")
	}

	err = s.createQCOW2Image(s.ImageAbsPath(name), s.ImageAbsPath(backingImageName), bytes)
	if err != nil {
		return nil, err
	}

	return s.Find(name)
}

func (s filesystemImageRepo) Copy(source string, destination string) (*api.Image, error) {
	var (
		sourceImage   *api.Image
		sourceAbsPath string
		err           error
	)

	// If source is an absolute path, try to copy the source
	// into the image repo. Otherwise, assume source is arlready
	// in the repo.
	if filepath.IsAbs(source) {
		fi, err := os.Stat(source)
		if err != nil {
			return nil, err
		}

		sourceImage, err = NewImage(fi)
		if err != nil {
			return nil, err
		}

		sourceAbsPath = source
	} else {
		sourceImage, err = s.Find(source)
		if err != nil {
			return nil, err
		}

		sourceAbsPath = s.ImageAbsPath(source)
	}

	if sourceImage == nil {
		return nil, errors.New("source image not found")
	}

	destinationImage, err := s.Create(destination, sourceImage.Size)
	if err != nil {
		return nil, err
	}

	in, err := os.Open(sourceAbsPath)
	if err != nil {
		return nil, err
	}
	defer in.Close()

	out, err := os.Create(s.ImageAbsPath(destination))
	if err != nil {
		return nil, err
	}
	defer out.Close()

	_, err = io.Copy(out, in)
	if err != nil {
		return nil, err
	}

	return destinationImage, nil
}
