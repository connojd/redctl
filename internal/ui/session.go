// Copyright © 2018 Assured Information Security, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ui

import (
	"log"
	"log/syslog"
	"os"
	"os/user"
	"path/filepath"

	"gitlab.com/redfield/redctl/api"
)

// Session represents a user session
type Session struct {
	client *api.Client

	appsDir string
}

// NewSession returns a new session
func NewSession(client *api.Client) (*Session, error) {
	user, err := user.Current()
	if err != nil {
		return nil, err
	}
	appsDir := filepath.Join(user.HomeDir, ".local/share/applications")

	s := &Session{
		client:  client,
		appsDir: appsDir,
	}

	return s, nil
}

// Start starts the user session
func (s *Session) Start() error {
	s.initialize()

	return s.eventHandler()
}

func (s *Session) initialize() {
	l, err := syslog.New(syslog.LOG_NOTICE, "redctl-session")
	if err != nil {
		log.Printf("Failed to set logger to syslog: %v", err)
	}
	log.SetOutput(l)

	// Best effort to initialize the user session
	err = os.MkdirAll(s.appsDir, 0755)
	if err != nil {
		log.Printf("Failed to create apps dir '%v': %v", s.appsDir, err)
	}

	err = s.initializeDock()
	if err != nil {
		log.Printf("Failed to initialize user session: %v", err)
	}

	// Leaving the mode empty will tell redctld to find the preferred mode
	// from xrandr.
	if err := s.client.DisplayForceResolution(""); err != nil {
		log.Printf("Failed to set display resolution: %v", err)
	}

	go func() {
		if err := s.startGuestDomains(); err != nil {
			log.Printf("Failed to start guest domains: %v", err)
		}
	}()
}

func (s *Session) eventHandler() error {
	stream, err := s.client.SubscribeEvents()
	if err != nil {
		return err
	}

	for {
		r, err := stream.Recv()
		if err != nil {
			return err
		}
		event := r.GetEvent()

		switch t := event.GetType(); t {
		case api.Event_DOMAIN_CREATED:
			d := event.GetDomain()

			if d.GetDesktopIcon() != "" {
				err := s.addDomainToDock(d)
				if err != nil {
					log.Printf("Failed to add desktop icon for %s", d.GetUuid())
				}
			}
		case api.Event_DOMAIN_REMOVED:
			d := event.GetDomain()

			if err := s.removeDomainFromDock(d); err != nil {
				log.Printf("Failed to remove desktop icon for %s", d.GetUuid())
			}
		default:
			log.Printf("received unknown event type: %v", t)
		}
	}
}

func (s *Session) startGuestDomains() error {
	domains, err := s.client.DomainFindAll()
	if err != nil {
		return err
	}

	for _, d := range domains {
		if !d.GetStartOnBoot() {
			continue
		}

		err := s.client.DomainStart(d.GetUuid())
		if err != nil {
			return err
		}
	}

	return nil
}
