// Copyright © 2018 Assured Information Security, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ui

import (
	"errors"
	"fmt"
	"io/ioutil"
	"os/exec"
	"path/filepath"
	"strings"

	"gitlab.com/redfield/redctl/api"
)

const desktopEntryTemplate = `
[Desktop Entry]
Name=%v
Icon=%v
Exec=redctl vm start --name %v
Terminal=false
Type=Application
StartupNotify=true
StartupWMClass=%v
`

const (
	dconfFavApps = "/org/gnome/shell/favorite-apps"
)

func desktopFileName(domain *api.Domain) string {
	return fmt.Sprintf("redfield-%v.desktop", domain.GetUuid())
}

func (s *Session) createDesktopEntry(domain *api.Domain) (string, error) {
	dfn := desktopFileName(domain)

	icon := domain.GetDesktopIcon()
	if icon == "" {
		return dfn, errors.New("no desktop icon specified")
	}

	name := domain.GetConfig().GetName()
	path := filepath.Join(s.appsDir, dfn)

	g, err := s.client.GraphicFind(icon, api.GraphicType_GRAPHIC_DESKTOP_ICON)
	if err != nil {
		return dfn, err
	}

	entry := fmt.Sprintf(desktopEntryTemplate, name, g.GetPath(), name, name)

	err = ioutil.WriteFile(path, []byte(entry), 0644)
	if err != nil {
		return dfn, fmt.Errorf("failed to write desktop file: %v", err)
	}

	return dfn, nil
}

func (s *Session) initializeDock() error {
	domains, err := s.client.DomainFindAll()
	if err != nil {
		return err
	}

	failed := make([]string, 0)

	for _, domain := range domains {
		if domain.GetDesktopIcon() == "" {
			continue
		}

		if err := s.addDomainToDock(domain); err != nil {
			failed = append(failed, domain.GetConfig().GetName())
		}
	}

	if len(failed) > 0 {
		return fmt.Errorf("failed to initialize desktop entries for %v", failed)
	}

	return nil
}

func (s *Session) addDomainToDock(domain *api.Domain) error {
	dfn, err := s.createDesktopEntry(domain)
	if err != nil {
		return fmt.Errorf("unable to create desktop icon for domain %v: %v", domain.GetConfig().GetName(), err)
	}

	return dconfAddFavorite(dfn)
}

func (s *Session) removeDomainFromDock(domain *api.Domain) error {
	dfn := desktopFileName(domain)

	return dconfRemoveFavorite(dfn)
}

func dconfRemoveFavorite(name string) error {
	favorites, err := dconfReadFavorites()
	if err != nil {
		return fmt.Errorf("failed to read existing favorites: %v", err)
	}

	// Find index of desktop entry
	index := -1

	for i, v := range favorites {
		if v == name {
			index = i
			break
		}
	}

	// Entry was not found, just return nil.
	if index == -1 {
		return fmt.Errorf("did not find desktop entry '%s'", name)
	}

	// Delete the entry from favorites.
	favorites = append(favorites[:index], favorites[index+1:]...)

	return dconfUpdateFavorites(favorites)
}

func dconfAddFavorite(name string) error {
	favorites, err := dconfReadFavorites()
	if err != nil {
		return fmt.Errorf("failed to read existing favorites: %v", err)
	}

	for _, f := range favorites {
		if name == f {
			// Desktop entry is already in favorites.
			return nil
		}
	}

	favorites = append(favorites, name)

	return dconfUpdateFavorites(favorites)
}

func dconfReadFavorites() ([]string, error) {
	favorites := make([]string, 0)

	cmd := exec.Command("dconf", "read", dconfFavApps)

	out, err := cmd.CombinedOutput()
	if err != nil {
		return favorites, fmt.Errorf("failed to read from %s (err=%v)", dconfFavApps, err)
	}
	s := string(out)

	// The output will be formatted like: ['fav1', 'fav2', 'fav3']\n
	// Make a []string with the items, not quotted.
	s = strings.TrimSpace(s)
	s = strings.Trim(s, "[]")

	for _, v := range strings.Split(s, ",") {
		v = strings.TrimSpace(v)
		v = strings.Trim(v, "'")

		favorites = append(favorites, v)
	}

	return favorites, nil
}

func dconfUpdateFavorites(favorites []string) error {
	// Format the argument in the syntax dconf expects.
	for i, v := range favorites {
		favorites[i] = fmt.Sprintf("'%s'", v)
	}
	v := fmt.Sprintf("[%v]", strings.Join(favorites, ","))

	cmd := exec.Command("dconf", "write", dconfFavApps, v)

	if err := cmd.Run(); err != nil {
		return fmt.Errorf("failed to write %s (err=%v)", dconfFavApps, err)
	}

	return nil
}
