// Copyright © 2018 Assured Information Security, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package api

import (
	"context"
	"log"
	"time"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
)

// Client is a client to the redctl service.
type Client struct {
	authUser              string
	authPassword          string
	serverURL             string
	timeout               *time.Duration
	tlsCaCertFile         string
	tlsCertFile           string
	tlsKeyFile            string
	tlsServerHostOverride string
	connected             bool

	grpcDialOptions []grpc.DialOption
	grpcContext     context.Context
	grpcCancelFunc  context.CancelFunc
	grpcClientConn  *grpc.ClientConn

	redctlClient RedctlClient
}

// unix sockets not compatible with TLS it appears
var secure = false

// GetRequestMetadata sets the value for "authorization" key
// Implemented interface for grpc.PerRPCCredentials
func (c *Client) GetRequestMetadata(context.Context, ...string) (map[string]string, error) {
	return map[string]string{"username": c.authUser, "password": c.authPassword}, nil
}

// RequireTransportSecurity - require transport security when we use creds
// Implemented interface for grpc.PerRPCCredentials
func (c *Client) RequireTransportSecurity() bool {
	return true
}

// NewClient with no options
func NewClient() *Client {
	return &Client{}
}

// WithAuthUser to specify auth username
func (c *Client) WithAuthUser(user string) *Client {
	c.authUser = user
	return c
}

// WithAuthPassword to specify auth password
func (c *Client) WithAuthPassword(password string) *Client {
	c.authPassword = password
	return c
}

// WithServerURL to specify server URL
func (c *Client) WithServerURL(url string) *Client {
	c.serverURL = url
	return c
}

// WithTimeout to specify connection timeout
func (c *Client) WithTimeout(timeout string) *Client {
	if timeout != "" {
		t, _ := time.ParseDuration(timeout)
		c.timeout = &t
	}
	return c
}

// RemoveTimeout removes the timeout from the client.
func (c *Client) RemoveTimeout() {
	c.timeout = nil
}

// WithTLSCaCertFile to specify ca cert file path
func (c *Client) WithTLSCaCertFile(path string) *Client {
	c.tlsCaCertFile = path
	return c
}

// WithTLSCertFile to specify device cert file path
func (c *Client) WithTLSCertFile(path string) *Client {
	c.tlsCertFile = path
	return c
}

// WithTLSKeyFile to specify device key file path
func (c *Client) WithTLSKeyFile(path string) *Client {
	c.tlsKeyFile = path
	return c
}

// WithTLSServerHostOverride to specify tls server host override
func (c *Client) WithTLSServerHostOverride(host string) *Client {
	c.tlsServerHostOverride = host
	return c
}

// configureGrpcDialOptions
func (c *Client) configureGrpcDialOptions() error {
	var opts []grpc.DialOption

	if secure {
		creds, err := credentials.NewClientTLSFromFile(c.tlsCaCertFile, c.tlsServerHostOverride)
		if err != nil {
			log.Fatalf("failed to create TLS credentials %v", err)
			return err
		}
		opts = append(opts, grpc.WithTransportCredentials(creds))
		opts = append(opts, grpc.WithPerRPCCredentials(c))
	} else {
		opts = append(opts, grpc.WithInsecure())
	}

	c.grpcDialOptions = opts
	return nil
}

// Context for grpc
func (c *Client) Context() context.Context {
	if c.timeout == nil {
		return context.Background()
	}

	c.grpcContext, c.grpcCancelFunc = context.WithTimeout(context.Background(), *c.timeout)

	return c.grpcContext
}

// Dial gRPC server
func (c *Client) Dial() error {
	if err := c.configureGrpcDialOptions(); err != nil {
		return err
	}

	conn, err := grpc.DialContext(c.Context(), c.serverURL, c.grpcDialOptions...)
	if err != nil {
		log.Printf("failed to dial: %v", err)
		return err
	}

	c.grpcClientConn = conn
	c.redctlClient = NewRedctlClient(conn)
	c.connected = true

	return nil
}

// RedctlClient getter
func (c *Client) RedctlClient() RedctlClient {
	return c.redctlClient
}

// Close connection to server
func (c *Client) Close() {
	if !c.connected {
		return
	}

	if c.grpcCancelFunc != nil {
		c.grpcCancelFunc()
	}

	if c.grpcClientConn != nil {
		c.grpcClientConn.Close()
	}

	c.redctlClient = nil
	c.connected = false
}

// Connected to server?
func (c *Client) Connected() bool {
	return c.connected
}
