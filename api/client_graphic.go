// Copyright © 2018 Assured Information Security, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package api

// GraphicImport imports a new graphic.
func (c *Client) GraphicImport(graphic *Graphic) error {
	r := &GraphicImportRequest{
		Graphic: graphic,
	}
	_, err := c.redctlClient.GraphicImport(c.Context(), r)

	return err
}

// GraphicRemove removes a graphic, if it exists.
func (c *Client) GraphicRemove(name string, kind GraphicType) error {
	r := &GraphicRemoveRequest{
		Graphic: &Graphic{
			Name: name,
			Type: kind,
		},
	}
	_, err := c.redctlClient.GraphicRemove(c.Context(), r)

	return err
}

// GraphicFind finds an existing graphic.
func (c *Client) GraphicFind(name string, kind GraphicType) (*Graphic, error) {
	r := &GraphicFindRequest{
		Graphic: &Graphic{
			Name: name,
			Type: kind,
		},
	}
	reply, err := c.redctlClient.GraphicFind(c.Context(), r)
	if err != nil {
		return nil, err
	}

	return reply.GetGraphic(), nil
}

// GraphicFindAll finds all graphics.
func (c *Client) GraphicFindAll() ([]*Graphic, error) {
	r := &GraphicFindAllRequest{}

	reply, err := c.redctlClient.GraphicFindAll(c.Context(), r)
	if err != nil {
		return nil, err
	}

	return reply.GetGraphics(), nil
}

// GraphicFindAllDesktopIcons finds all desktop icon graphics.
func (c *Client) GraphicFindAllDesktopIcons() ([]*Graphic, error) {
	r := &GraphicFindAllRequest{
		Type: GraphicType_GRAPHIC_DESKTOP_ICON,
	}

	reply, err := c.redctlClient.GraphicFindAll(c.Context(), r)
	if err != nil {
		return nil, err
	}

	return reply.GetGraphics(), nil
}

// GraphicFindAllBackgrounds finds all background graphics.
func (c *Client) GraphicFindAllBackgrounds() ([]*Graphic, error) {
	r := &GraphicFindAllRequest{
		Type: GraphicType_GRAPHIC_BACKGROUND,
	}

	reply, err := c.redctlClient.GraphicFindAll(c.Context(), r)
	if err != nil {
		return nil, err
	}

	return reply.GetGraphics(), nil
}
