// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package api

import (
	"fmt"
	"reflect"
	"strings"
)

// MarshalText formats a DomainUsbController into an xl spec string.
func (duc DomainUsbController) MarshalText() ([]byte, error) {
	var spec []string

	t := reflect.TypeOf(duc)
	for i := 0; i < t.NumField(); i++ {
		// JSON tags are of form `json:"<name>, <opts>", use
		// name as key in xl.cfg format.
		key := t.Field(i).Tag.Get("json")
		key = strings.Split(key, ",")[0]

		// '-' means to ignore when marshaling
		if key == "-" {
			continue
		}

		val := reflect.ValueOf(&duc).Elem().Field(i)

		// Skip unexported fields
		if !val.CanInterface() {
			continue
		}

		// Skip zero-value fields
		if val.Interface() == reflect.Zero(val.Type()).Interface() {
			continue
		}

		// If we make it here, append the key-value pair to the spec string.
		spec = append(spec, fmt.Sprintf("%v=%v", key, val.Interface()))
	}

	return []byte(strings.Join(spec, ",")), nil
}

// UnmarshalText populates a DomainUsbController from an xl spec string.
func (duc *DomainUsbController) UnmarshalText(text []byte) error {
	// If the string is empty, don't populate any fields
	if string(text) == "" {
		return nil
	}

	// Create a map to store key-value pairs.
	specs := make(map[string]string)

	for _, spec := range strings.Split(string(text), ",") {
		s := strings.Split(spec, "=")

		if len(s) != 2 {
			return fmt.Errorf("expected spec string format '<key>=<value>'")
		}

		// s[0] is the key, s[1] is the value.
		specs[s[0]] = s[1]
	}

	t := reflect.TypeOf(*duc)
	for i := 0; i < t.NumField(); i++ {
		// Get field name from JSON tag
		name := strings.Split(t.Field(i).Tag.Get("json"), ",")[0]

		if val, ok := specs[name]; ok {
			reflect.ValueOf(duc).Elem().Field(i).SetString(val)
		}
	}

	return nil
}
