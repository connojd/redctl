// Copyright 2019 Assured Information Security, Inc.

import React, {Component} from 'react';

import WebsocketClient from '../websocket-client';

import VMWizard from './vmwizard';
import SubMenu from './submenu';
import Row from './row';
import Button from './button';

const style = {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center'
};

const subMenuStyle = {
    width: '75%',
};

// Default component state before the user begins navigating
const emptyState = {
    error: undefined
};

// VMenu represents the parent menu
export default class VMMenu extends Component {
    constructor(props) {
        super(props);

        this.bindEvents = this.bindEvents.bind(this);
        this.close = this.close.bind(this);
        this.reset = this.reset.bind(this);
        this.clearError = this.clearError.bind(this);
        this.toMainMenu = this.toMainMenu.bind(this);
        this.handleNew = this.handleNew.bind(this);
        this.handleCreate = this.handleCreate.bind(this);
        this.handleImage = this.handleImage.bind(this);
        this.handleUpdate = this.handleUpdate.bind(this);

        // Instantiate WebSocket client, bind event handlers
        let client = this.bindEvents();

        this.state = {
            ...emptyState,
            client
        };
    }

    // bind WebSockets events
    bindEvents() {
        let client = new WebsocketClient();

        client.handle('error', ((msg) => {
            this.setState((state) => ({ ...state, error: msg.data, status: undefined }));

            console.error(msg.data);
        }).bind(this));

        client.handle('backends', ((msg) => {
            this.setState((state) => ({ ...state, backends: msg.data }));
        }).bind(this));

        client.handle('images', ((msg) => {
            this.setState((state) => ({ ...state, images: msg.data }));
        }).bind(this));

        client.handle('icons', ((msg) => {
            this.setState((state) => ({ ...state, icons: msg.data }));
        }).bind(this));

        client.handle('create', ((msg) => {
            this.setState((state) => ({ ...state, status: undefined }));
        }).bind(this));

        client.handle('image', ((msg) => {
            this.setState((state) => ({
                ...state, status: undefined,
                imagePath: msg.data,
                images: [...this.state.images, msg.data]
            }));
        }).bind(this));

        client.handle('update', ((msg) => {
            this.setState((state) => ({
                ...state,
                status: 'success',
                statusMessage: 'Successfully created VM'
            }));
        }).bind(this));

        client.receive();

        return client;
    }

    // Reset state that affects either the manual/import configuration menus
    reset() {
        this.setState((state) => ({
            ...state,
            ...emptyState
        }));
    }

    // Clear error message from backend
    clearError() {
        this.setState((state) => ({ ...state, error: undefined }));
    }

    // Close the Electron window
    close() {
        this.state.client.sendEvent('done', undefined);
    }

    // Return from a sub-menu to the main menu
    toMainMenu() {
        this.setState((state) => ({ ...state, choice: undefined }));
    }

    handleNew() {
        this.setState((state) => ({ ...state, choice: 'new' }));
    }

    handleCreate(data) {
        this.state.client.sendEvent('create', data);

        this.setState((state) => ({
            ...state,
            status: 'loading',
            statusMessage: 'Creating VM: ' + data.name
        }));
    }

    handleImage(data) {
        this.state.client.sendEvent('image', data);

        this.setState((state) => ({
            ...state,
            status: 'loading',
            statusMessage: 'Creating disk image'
        }));
    }

    handleUpdate(data) {
        this.state.client.sendEvent('update', data);

        this.setState((state) => ({
            ...state,
            status: 'loading',
            statusMessage: 'Applying VM settings'
        }));
    }

    render() {
        let component = (
            <div className={'submenu'} style={style}>
                <SubMenu style={subMenuStyle}>
                    <h4>{'Select "New" to create a VM manually'}</h4>
                    <Button onClick={this.handleNew}>New</Button>
                </SubMenu>
            </div>
        );

        if (this.state.choice === 'new') {
            component = (
                <VMWizard
                    reset={this.reset}
                    clearError={this.clearError}
                    close={this.close}
                    error={this.state.error}
                    toMainMenu={this.toMainMenu}
                    handleCreate={this.handleCreate}
                    handleImage={this.handleImage}
                    handleUpdate={this.handleUpdate}
                    status={this.state.status}
                    statusMessage={this.state.statusMessage}
                    backends={this.state.backends}
                    images={this.state.images}
                    imagePath={this.state.imagePath}
                    icons={this.state.icons}
                />
            );
        }

        return component;
    }
}
