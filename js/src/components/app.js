// Copyright 2019 Assured Information Security, Inc.

import React, {Component} from 'react';

import VMMenu from './vmmenu';

export default class App extends Component {
    constructor(props) {
        super(props);

        this.state = {};
    }

    render() {
        return (
            <VMMenu />
        );
    }
}
