// Copyright 2019 Assured Information Security, Inc.

import React from 'react';

const Button = (props) => (
    <button {...props} className={'button'}>{props.children}</button>
);

export default Button;
