module gitlab.com/redfield/redctl

require (
	github.com/Equanox/gotron v0.2.23
	github.com/ghodss/yaml v1.0.0 // indirect
	github.com/gofrs/flock v0.7.1
	github.com/golang/protobuf v1.3.0
	github.com/google/uuid v1.1.1
	github.com/jaypipes/ghw v0.0.0-20190131082148-032dfb1f8cb2 // indirect
	github.com/jaypipes/pcidb v0.0.0-20190216134740-adf5a9192458 // indirect
	github.com/jcelliott/lumber v0.0.0-20160324203708-dd349441af25 // indirect
	github.com/mitchellh/go-homedir v1.1.0 // indirect
	github.com/nanobox-io/golang-scribble v0.0.0-20180621225840-336beac0a992
	github.com/pkg/errors v0.8.1
	github.com/rs/xid v1.2.1
	github.com/spf13/cobra v0.0.3
	github.com/spf13/viper v1.3.1
	golang.org/x/crypto v0.0.0-20181203042331-505ab145d0a9
	google.golang.org/grpc v1.19.0
	howett.net/plist v0.0.0-20181124034731-591f970eefbb // indirect
)

go 1.11
