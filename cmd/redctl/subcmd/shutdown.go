// Copyright © 2018 Assured Information Security, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"log"

	"github.com/spf13/cobra"
)

var (
	shutdownCmd = &cobra.Command{
		Use:   "shutdown",
		Short: "Shutdown redctl",
		Long: `Shutdown all running domains and perform other opertaions
		needed to cleanup on shutdown`,
		PreRun: func(cmd *cobra.Command, args []string) {
			preRunFlagsFixup(cmd)
		},
		Run: func(cmd *cobra.Command, args []string) {
			client := initializeRedctlClient()

			timeout, err := cmd.Flags().GetUint32("timeout")
			if err != nil {
				log.Fatalf("Unexpected error parsing timeout flag: %v", err)
			}

			if err := client.Shutdown(timeout); err != nil {
				log.Printf("Failed to shutdown redctl: %v", err)
			}
		},
	}
)

func init() {
	rootCmd.AddCommand(shutdownCmd)
	shutdownCmd.Flags().Uint32("timeout", 60, "timeout for shutting down domains before destroying them")
}
