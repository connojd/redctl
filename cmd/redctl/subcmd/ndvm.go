// Copyright © 2018 Assured Information Security, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"log"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// ndvmCmd represents the ndvm command
var (
	ndvmCmd = &cobra.Command{
		Use:   "ndvm",
		Short: "Manage network virtual machines (NDVMs)",
		Long:  ``,
	}
)

var (
	ndvmCreateCmd = &cobra.Command{
		Use:   "create",
		Short: "Create a network domain VM",
		Long:  ``,
		PreRun: func(cmd *cobra.Command, args []string) {
			preRunFlagsFixup(cmd)
		},
		Run: func(cmd *cobra.Command, args []string) {
			client := initializeRedctlClient()
			defer client.Close()

			name := viper.GetString("name")
			if name == "" {
				log.Fatal("Domain name is required for creation")
			}

			image := viper.GetString("image")
			if image == "" {
				image = "ndvm.raw"
			}

			assignAll := viper.GetBool("assign-all")
			copyImage := viper.GetBool("copy-image")

			if err := client.CreateNDVM(name, image, assignAll, copyImage); err != nil {
				log.Fatalf("Failed to create NDVM: %v", err)
			}
		},
	}
)

func init() {
	rootCmd.AddCommand(ndvmCmd)

	ndvmCmd.AddCommand(ndvmCreateCmd)
	ndvmCreateCmd.Flags().String("name", "", "ndvm name")
	ndvmCreateCmd.Flags().String("image", "", "image name")
	ndvmCreateCmd.Flags().Bool("assign-all", false, "assign all PCI network devices to newly created ndvm")
	ndvmCreateCmd.Flags().Bool("copy-image", true, "Make a copy of the base image for the NDVM's disk")
}
