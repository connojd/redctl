// Copyright © 2018 Assured Information Security, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"log"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// upgradeCmd provides upgrade functionality for the client
var (
	upgradeCmd = &cobra.Command{
		Use:   "upgrade",
		Short: "Upgrade the control domain",
		Long:  `Update the Redfield version on the end user device`,
		PreRun: func(cmd *cobra.Command, args []string) {
			preRunFlagsFixup(cmd)
		},
		Run: func(cmd *cobra.Command, args []string) {
			log.Println("Starting upgrade...")
			client := initializeRedctlClient()
			name := viper.GetString("image")

			if name == "" {
				log.Fatal("Invalid image name provided.")
			} else {
				_, err := client.UpgradeControlDomain(name)
				if err != nil {
					log.Fatalf("Failed to upgrade control domain: %v", err)
				}
			}
		},
	}
)

func init() {
	rootCmd.AddCommand(upgradeCmd)
	upgradeCmd.Flags().String("image", "", "Upgrade the control domain using the specified image name")
}
